from django.shortcuts import render_to_response
from django.template  import RequestContext

# Create your views here. 

def suivi(request):
    return render_to_response('suivi.html', RequestContext(request))
