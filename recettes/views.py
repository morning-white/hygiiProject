from django.shortcuts import render, redirect, render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from recettes.forms import AlimentForm, RecetteForm
from recettes.models import Aliment, type_aliment, mesurande

# Create your views here.

def recettes(request):
  if request.method == 'POST':
    return render_to_response('recettes.html',RequestContext(request))






def add_aliment(request):
  if request.method == 'POST':
    add_aliment = AlimentForm(request.POST)
    if add_aliment.is_valid():
      name = request.POST.get('name')
      type_aliment_id = request.POST.get('type_aliment_id')
      mesurande_id = request.POST.get('mesurande_id')
      calories = request.POST.get('calories')
      proteines = request.POST.get('proteines')

      t_a = type_aliment.objects.get(id=type_aliment_id);
      m = mesurande.objects.get(id=mesurande_id)

      a = Aliment(name = name,type_aliment_id =t_a,mesurande_id=m,calories=calories,proteines=proteines)
      a.save()
      #render_to_response(type_aliment_id,RequestContext(request))

      add_aliment = AlimentForm()
      return render_to_response("add_aliment.html",
                              {'form_aliment':add_aliment,},RequestContext(request))
      #return HttpResponseRedirect("/add_aliment")
     # html = "<html><body>{!s} </html></body>".format(type_aliment_id)

     # return HttpResponse(html) # Redirect after POST

  else:
    add_aliment = AlimentForm()
  return render_to_response("add_aliment.html",
                              {'form_aliment':add_aliment,},RequestContext(request))

def add_recette(request):
  if request.method == 'POST':
    add_recette = RecetteForm(request.POST)
    if add_recette.is_valid():
      name = request.POST.get('name')
      type_aliment_id = request.POST.get('type_aliment_id')
      mesurande_id = request.POST.get('mesurande_id')
      calories = request.POST.get('calories')
      proteines = request.POST.get('proteines')

      t_a = type_aliment.objects.get(id=type_aliment_id);
      m = mesurande.objects.get(id=mesurande_id)

      a = Aliment(name = name,type_aliment_id =t_a,mesurande_id=m,calories=calories,proteines=proteines)
      a.save()
      #render_to_response(type_aliment_id,RequestContext(request))

      add_recette = AlimentForm()
      return render_to_response("add_recette.html",
                              {'form_recette':add_recette,},RequestContext(request))
      #return HttpResponseRedirect("/add_recette")
     # html = "<html><body>{!s} </html></body>".format(type_aliment_id)

     # return HttpResponse(html) # Redirect after POST

  else:
    add_recette = RecetteForm()
  return render_to_response("add_recette.html",
                              {'form_recette':add_recette,},RequestContext(request))