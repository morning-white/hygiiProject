from django.db import models

# Create your models here.


class type_aliment(models.Model):
  id = models.AutoField(primary_key=True)
  name = models.CharField(max_length=200)

  def __unicode__(self):
    return self.name

class mesurande(models.Model):
  id = models.AutoField(primary_key=True)
  name = models.CharField(max_length=200)

  def __unicode__(self):
    return self.name

class Aliment(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    type_aliment_id = models.OneToOneField(type_aliment)
    mesurande_id = models.OneToOneField(mesurande)
    calories = models.IntegerField(default=0)
    proteines = models.IntegerField(default=0)


    def __unicode__(self):
      return self.name


class Recette(models.Model):
  id = models.AutoField(primary_key=True)
  name = models.CharField(max_length=200)
  ingredients = models.ManyToManyField(Aliment)
  description = models.CharField(max_length=5000)
  temps_preparation = models.IntegerField(default=0)
  temps_cuisson = models.IntegerField(default=0)
  difficulte = models.IntegerField(default=3)
  date_creation = models.IntegerField(default=0)
  note = models.IntegerField(default=3)
  photo = models.ImageField()

