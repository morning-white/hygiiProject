from django import forms
from django.forms import ModelForm
from recettes.models import Aliment, Recette, type_aliment

class AlimentForm(ModelForm):
  class Meta:
    model=Aliment
    field = ('name','type_aliment_id','mesurande_id','calories','proteines')


class RecetteForm(ModelForm):
  class Meta:
    model=Recette
    field = ('name','ingredients','id','description','temps_preparation','temps_cuisson','difficulte','date_creation','note','photo')
