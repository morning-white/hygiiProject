# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Aliment',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('type_aliment', models.CharField(max_length=200)),
                ('sodium', models.FloatField(default=0)),
                ('fer', models.FloatField(default=0)),
                ('proteine', models.FloatField(default=0)),
                ('glucide', models.FloatField(default=0)),
                ('sucre', models.FloatField(default=0)),
                ('lipide', models.FloatField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
