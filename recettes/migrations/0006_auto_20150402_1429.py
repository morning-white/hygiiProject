# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recettes', '0005_aliment_mesurande_recette_type_aliment'),
    ]

    operations = [
        migrations.RenameField(
            model_name='aliment',
            old_name='mesurande',
            new_name='mesurande_id',
        ),
        migrations.RenameField(
            model_name='aliment',
            old_name='type_aliment',
            new_name='type_aliment_id',
        ),
    ]
