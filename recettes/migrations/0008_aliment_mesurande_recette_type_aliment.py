# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recettes', '0007_auto_20150402_1438'),
    ]

    operations = [
        migrations.CreateModel(
            name='mesurande',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Aliment',
            fields=[
                ('id', models.AutoField(primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('type_aliment_id', models.OneToOneField(primary_key=True, serialize=False, to='recettes.type_aliment')),
                ('mesurande_id', models.OneToOneField(primary_key=True, to='recettes.mesurande')),
                ('calories', models.IntegerField(default=0)),
                ('proteines', models.IntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Recette',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.CharField(max_length=5000)),
                ('temps_preparation', models.IntegerField(default=0)),
                ('temps_cuisson', models.IntegerField(default=0)),
                ('difficulte', models.IntegerField(default=3)),
                ('date_creation', models.IntegerField(default=0)),
                ('note', models.IntegerField(default=3)),
                ('photo', models.ImageField(upload_to=b'')),
                ('ingredients', models.ManyToManyField(to='recettes.Aliment')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='type_aliment',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
