# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recettes', '0008_aliment_mesurande_recette_type_aliment'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='aliment',
            name='mesurande_id',
        ),
        migrations.RemoveField(
            model_name='aliment',
            name='type_aliment_id',
        ),
        migrations.DeleteModel(
            name='mesurande',
        ),
        migrations.RemoveField(
            model_name='recette',
            name='ingredients',
        ),
        migrations.DeleteModel(
            name='Aliment',
        ),
        migrations.DeleteModel(
            name='Recette',
        ),
        migrations.DeleteModel(
            name='type_aliment',
        ),
    ]
