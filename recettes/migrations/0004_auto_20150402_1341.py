# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recettes', '0003_aliment_recette'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='recette',
            name='ingredients',
        ),
        migrations.DeleteModel(
            name='Aliment',
        ),
        migrations.DeleteModel(
            name='Recette',
        ),
    ]
