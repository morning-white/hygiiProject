BEGIN;


 INSERT INTO recettes_type_aliment (name) VALUES('produit laitier');
 INSERT INTO recettes_type_aliment (name) VALUES('VPO');
 INSERT INTO recettes_type_aliment (name) VALUES('feculent');
 INSERT INTO recettes_type_aliment (name) VALUES('produit sucré');
 INSERT INTO recettes_type_aliment (name) VALUES('legume');
 INSERT INTO recettes_type_aliment (name) VALUES('corps gras');
 INSERT INTO recettes_type_aliment (name) VALUES('boisson');
 INSERT INTO recettes_type_aliment (name) VALUES('fruit');
 INSERT INTO recettes_type_aliment (name) VALUES('condiment');

 INSERT INTO recettes_mesurande (name) VALUES('unite');
 INSERT INTO recettes_mesurande (name) VALUES('mg');
 INSERT INTO recettes_mesurande (name) VALUES('ml');
 INSERT INTO recettes_mesurande (name) VALUES('pincee');
 INSERT INTO recettes_mesurande (name) VALUES('cuillere a soupe');
 INSERT INTO recettes_mesurande (name) VALUES('cuillere a cafe');


COMMIT;
