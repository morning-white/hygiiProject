from django import forms
from django.forms import ModelForm
from inscription.models import Utilisateur
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class UserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('last_name','first_name','username', 'email', )

class UtilisateurForm(ModelForm):
    class Meta:
        model = Utilisateur
        exclude = ('user',)

