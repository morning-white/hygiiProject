from django import forms
from django.forms import ModelForm
from inscription.models import Utilisateur
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthentificationForm


class UserAuthentForm(AuthentificationForm):
