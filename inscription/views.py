from django.shortcuts import render, redirect, render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
#from django.contrib.auth.models import User
#from django import forms
from inscription.forms import UserForm, UtilisateurForm
from django.contrib.auth.forms import AuthenticationForm
from models import Utilisateur
from django.contrib.auth import login, authenticate, logout

def register_view(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST)
        utilisateur_form = UtilisateurForm(request.POST)
        if user_form.is_valid and utilisateur_form.is_valid():
            u = Utilisateur(**utilisateur_form.cleaned_data)
            u.user = user_form.save()
            u.save()
            return HttpResponseRedirect("/accueil")

    else:
        user_form = UserForm()
        utilisateur_form = UtilisateurForm()

    return render_to_response("inscription.html",
                              {'user_form':user_form, 'utilisateur_form': utilisateur_form,}, RequestContext(request))


def login_user(request):
    if request.method =='POST':
        auth_form=AuthenticationForm(data=request.POST)
        if auth_form.is_valid():
           username = request.POST.get('username')
           password = request.POST.get('password')
           uti = authenticate(username = username,password = password)

           if uti:
            # Is the account active? It could have been disabled.
            if uti.is_active:
                # If the account is valid and active, we can log the user in.
                # We'll send the user back to the homepage.
                login(request, uti)
                return HttpResponseRedirect('/accueil')
            else:
                # An inactive account was used - no logging in!
                return HttpResponse("Your account is disabled.")
        else:
            # Bad login details were provided. So we can't log the user in.
            return HttpResponse("erreur nom d'utilisateur ou mot de passe ou votre compte a ete desactive")

    else:
        auth_form=AuthenticationForm()
        return HttpResponseRedirect('/accueil')

def logout_user(request):
    logout(request)
    return HttpResponseRedirect('/accueil')