from django.contrib.auth.models import User
from django.db import models

class Utilisateur(models.Model):
    #User_Auth de Django
    user = models.ForeignKey(User, null=True, blank=True, unique=True)
    
    #Ajout de nos champs perso
    Age = models.IntegerField()                                                                                      
    Sexe = models.BooleanField(default=1)