from django.shortcuts import render_to_response
from django.template  import RequestContext

# Create your views here.

def exercices(request):
    return render_to_response('exercices.html',RequestContext(request))
