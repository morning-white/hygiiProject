from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.contrib.auth.views import login

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'hygiiProject.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^accueil/', 'accueil.views.accueil', name='accueil'),
    url(r'^recettes/', 'recettes.views.recettes', name='recettes'),
    url(r'^courses/', 'courses.views.courses', name='courses'),
    url(r'^exercices/', 'exercices.views.exercices', name='exercices'),
    url(r'^suivi/', 'suivi.views.suivi', name='suivi'),
    url(r'^inscription/', 'inscription.views.register_view'),
    #url(r'^authenticate/', 'inscription.views.login_user'),
    url(r'^login/$', 'inscription.views.login_user', name = 'auth_login'),
    url(r'^logout/$', 'inscription.views.logout_user', name = 'auth_logout'),
    url(r'^add_aliment/', 'recettes.views.add_aliment', name='add_aliment'),
    url(r'^add_recette/', 'recettes.views.add_recette', name='add_recette'),
)

# urlpatterns += staticfiles_urlpatterns()
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# if settings.DEBUG:
#     urlpatterns += patterns('',
#         (r'^site-media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT }),
#     )
