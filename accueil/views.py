from django.shortcuts import render_to_response
from django.template  import RequestContext

# Create your views here.  

def accueil(request):
    return render_to_response('index.html',RequestContext(request))
