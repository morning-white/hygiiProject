from django.shortcuts import render_to_response
from django.template  import RequestContext

# Create your views here.

def courses(request):
    return render_to_response('courses.html',RequestContext(request))
